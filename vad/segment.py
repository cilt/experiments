#! /usr/bin/python

## Adapted from https://github.com/wiseman/py-webrtcvad/blob/master/example.py

import collections
import contextlib
import sys
import wave

import webrtcvad

def read_wave(path):
    with contextlib.closing(wave.open(path, 'rb')) as wf:
        num_channels = wf.getnchannels()
        assert num_channels == 1
        sample_width = wf.getsampwidth()
        assert sample_width == 2
        sample_rate = wf.getframerate()
        assert sample_rate in (8000, 16000, 32000)
        pcm_data = wf.readframes(wf.getnframes())
        return pcm_data, sample_rate


def write_wave(path, audio, sample_rate):
    with contextlib.closing(wave.open(path, 'wb')) as wf:
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(sample_rate)
        wf.writeframes(audio)


def frame_generator(frame_duration_ms, audio, sample_rate):
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    while offset + n < len(audio):
        yield audio[offset:offset + n]
        offset += n


def vad_collector(sample_rate, frame_duration_ms,
                  padding_duration_ms, vad, frames):
    num_padding_frames = int(padding_duration_ms / frame_duration_ms)
    ring_buffer = collections.deque(maxlen=num_padding_frames)
    triggered = False
    voiced_frames = []
    for frame in frames:
        voiced = vad.is_speech(frame, sample_rate)
        if verbose:
            sys.stdout.write('1' if voiced else '0')
        if not triggered:
            ring_buffer.append(frame)
            num_voiced = len([f for f in ring_buffer
                              if vad.is_speech(f, sample_rate)])
            if num_voiced > 0.9 * ring_buffer.maxlen:
                if verbose:
                    sys.stdout.write('+')
                triggered = True
                voiced_frames.extend(ring_buffer)
                ring_buffer.clear()
        else:
            voiced_frames.append(frame)
            ring_buffer.append(frame)
            num_unvoiced = len([f for f in ring_buffer
                                if not vad.is_speech(f, sample_rate)])
            if num_unvoiced > 0.9 * ring_buffer.maxlen:
                if verbose:
                    sys.stdout.write('-')
                triggered = False
                yield b''.join(voiced_frames)
                ring_buffer.clear()
                voiced_frames = []
    sys.stdout.write('\n')
    if voiced_frames:
        yield b''.join(voiced_frames)

    print 'Finished\n'

def main(args):
    if len(args) != 3:
        sys.stderr.write(
            'Usage: example.py <aggressiveness> <path to wav file> <verbose>\n')
        sys.exit(1)

    global verbose
    verbose = (args[2] == "true")
    print 'Verbose: ', verbose

    frame_width = 20
    audio, sample_rate = read_wave(args[1])
    vad = webrtcvad.Vad(int(args[0]))
    frames = frame_generator(frame_width, audio, sample_rate)
    frames = list(frames)
    segments = vad_collector(sample_rate, frame_width, 500, vad, frames)
    segment_start_ms = 0
    min_start_duration = 10000
    min_seg_duration = 600000

    for i, segment in enumerate(segments):
        path = 'chunk-%002d.wav' % (i,)
        segment_duration_ms = len(segment) * 1000 / (sample_rate * 2)

        if verbose:
            print('%d from %d duration %d ms' % (i,segment_start_ms,segment_duration_ms,))

        if (i==0) and (segment_duration_ms > min_start_duration):
            print('## start %s from %d duration %d ms' % (path,segment_start_ms,segment_duration_ms,))
            write_wave(path, segment, sample_rate)

        if (i>1) and (segment_duration_ms > min_seg_duration):
            print('## middle %s from %d duration %d ms' % (path,segment_start_ms,segment_duration_ms,))
            # write_wave(path, segment, sample_rate)

        segment_start_ms += segment_duration_ms

if __name__ == '__main__':
    main(sys.argv[1:])

